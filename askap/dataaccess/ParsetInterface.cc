/// @file
/// @brief A method to set up converters and selectors from parset file
/// @details Parameters are currently passed around using parset files.
/// The methods declared in this file set up converters and selectors
/// from the ParameterSet object. This is probably a temporary solution.
/// This code can eventually become a part of some class (e.g. a DataSource
/// which returns selectors and converters with the defaults alread
/// applied according to the parset file).
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>
///

#include <askap/dataaccess/ParsetInterface.h>
#include <askap_accessors.h>
#include <askap/askap/AskapLogging.h>
ASKAP_LOGGER(logger, "");

#include <askap/askap/AskapError.h>
#include <askap/dataaccess/DataAccessError.h>
#include <askap/dataaccess/TableDataSelector.h>

#include <iostream>

// @brief set selections according to the given parset object
// @details
// @param[in] sel a shared pointer to the converter to be updated
// @param[in] parset a parset object to read the parameters from
void askap::accessors::operator<<(const boost::shared_ptr<IDataSelector> &sel,
                 const LOFAR::ParameterSet &parset)
{
  ASKAPDEBUGASSERT(sel);
  // do tile selection first, other selections may affect #rows/tile and break the tile selection
  // also - filter out the auto setting (handled elsewhere)
  if (parset.isDefined("Tiles") && parset.getString("Tiles","")!="auto") {
      std::vector<LOFAR::uint32> tiles = parset.getUint32Vector("Tiles",true);
      if (tiles.size() != 2) {
          ASKAPTHROW(DataAccessError,"The 'Tiles' parameter in the Parset should have "
	          "exactly 2 elements, the current parameter has "<<tiles.size()<<" elements");
      }
      // Tiles is Table specific, so cast to TableDataSelector
      boost::shared_ptr<TableDataSelector> tsel = boost::dynamic_pointer_cast<TableDataSelector>(sel);
      if (tsel) {
          tsel->chooseDataTiles(static_cast<casacore::uInt>(tiles[0]),
                static_cast<casacore::uInt>(tiles[1]));
      } else {
          ASKAPTHROW(DataAccessError,"Tile selection can only be used for data stored in Tables");
      }
  }
  if (parset.isDefined("Feed")) {
      ASKAPCHECK(!parset.isDefined("Beam"), "Both 'Feed' and 'Beam' should not be defined simultaneously!");
      sel->chooseFeed(static_cast<casacore::uInt>(parset.getUint32("Feed")));
  }
  if (parset.isDefined("Beam")) {
      ASKAPCHECK(!parset.isDefined("Feed"), "Both 'Feed' and 'Beam' should not be defined simultaneously!");
      sel->chooseFeed(static_cast<casacore::uInt>(parset.getUint32("Beam")));
  }
  if (parset.isDefined("Baseline")) {
      std::vector<LOFAR::uint32> baseline = parset.getUint32Vector("Baseline");
      if (baseline.size() != 2) {
          ASKAPTHROW(DataAccessError,"The 'Baseline' parameter in the Parset should have "
	          "exactly 2 elements, the current parameter has "<<baseline.size()<<" elements");
      }
      sel->chooseBaseline(static_cast<casacore::uInt>(baseline[0]),
                          static_cast<casacore::uInt>(baseline[1]));
  }
  if (parset.isDefined("Antenna")) {
      sel->chooseAntenna(static_cast<casacore::uInt>(parset.getUint32("Antenna")));
  }
  if (parset.isDefined("Channels")) {
      std::vector<LOFAR::uint32> chans = parset.getUint32Vector("Channels");
      if ((chans.size() != 2) && (chans.size() != 3)) {
          ASKAPTHROW(DataAccessError,"The 'Channels' parameter in the Parset should have "
	    "2 or 3 elements, the current parameter has "<<chans.size()<<" elements");
      }
      sel->chooseChannels(static_cast<casacore::uInt>(chans[0]),
                          static_cast<casacore::uInt>(chans[1]),
	     chans.size() == 3 ? static_cast<casacore::uInt>(chans[2]) : 1);
  }
  if (parset.isDefined("SpectralWindow")) {
      sel->chooseSpectralWindow(static_cast<casacore::uInt>(
                    parset.getUint32("SpectralWindow")));
  }
  if (parset.isDefined("Polarizations")) {
      sel->choosePolarizations(parset.getString("Polarisations"));
  }
  if (parset.isDefined("Cycles")) {
      std::vector<LOFAR::uint32> cycles = parset.getUint32Vector("Cycles");
      if (cycles.size() != 2) {
          ASKAPTHROW(DataAccessError,"The 'Cycles' parameter in the Parset should have "
	          "exactly 2 elements, the current parameter has "<<cycles.size()<<" elements");
      }
      sel->chooseCycles(static_cast<casacore::uInt>(cycles[0]),
                        static_cast<casacore::uInt>(cycles[1]));
  }
  if (parset.isDefined("TimeRange")) {
      std::vector<double> timeRange = parset.getDoubleVector("TimeRange");
      if (timeRange.size() != 2) {
          ASKAPTHROW(DataAccessError,"The 'TimeRange' parameter in the Parset should have "
	          "exactly 2 elements, the current parameter has "<<timeRange.size()<<" elements");
      }
      sel->chooseTimeRange(static_cast<casacore::Double>(timeRange[0]),
                        static_cast<casacore::Double>(timeRange[1]));
  }
  if (parset.isDefined("CorrelationType")) {
      std::string corrType=parset.getString("CorrelationType");
      if (corrType == "auto") {
          sel->chooseAutoCorrelations();
      } else if (corrType == "cross") {
          sel->chooseCrossCorrelations();
      } else if (corrType != "all") {
          ASKAPTHROW(DataAccessError, "CorrelationType can either be cross, auto or all (default)");
      }
  }
  if (parset.isDefined("MinUV")) {
      sel->chooseMinUVDistance(parset.getDouble("MinUV"));
  }
  if (parset.isDefined("MinNonZeroUV")) {
      sel->chooseMinNonZeroUVDistance(parset.getDouble("MinNonZeroUV"));
  }
  if (parset.isDefined("MaxUV")) {
      sel->chooseMaxUVDistance(parset.getDouble("MaxUV"));
  }
  if (parset.isDefined("ScanNumber")) {
      sel->chooseScanNumber(static_cast<casacore::uInt>(parset.getUint32("ScanNumber")));
  }
}
